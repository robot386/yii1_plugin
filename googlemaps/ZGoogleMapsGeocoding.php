<?php

class ZGoogleMapsGeocoding extends CApplicationComponent
{
	const BASE_URL = 'http://maps.google.com/maps/api/geocode/json?address=';
	
	public $latitude;
	
	public $longitude;
	
	public $error;		
	
	public function coordinatesByAddress($address, $city)
	{	
		if ($address == null || $city == null)
		{
			return false;
		}
		
		$query = $address.', '.$city;

		$url = self::BASE_URL.urlencode($query).'&sensor=false';

		$request = curl_init();

		curl_setopt($request, CURLOPT_URL, $url);
		curl_setopt($request, CURLOPT_HEADER, 0);    
		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);

		$response = CJSON::decode(curl_exec($request));
		
		if ($response['status'] == 'OK')
		{
			$geolocation = $response['results'][0]['geometry']['location'];

			$this->latitude = $geolocation['lat'];
			$this->longitude = $geolocation['lng'];

			$this->error = null;

			return true;
		}
		else
		{	
			$this->latitude = null;
			$this->longitude = null;

			$this->error = $json->status;

			return false;
		}
	}
}

?>
