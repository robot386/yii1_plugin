<?php

class ZOpenWeather extends CApplicationComponent
{
	const BASE_URL = 'http://api.openweathermap.org/data/2.5/';
	
	const FUNCTION_CURRENT = 'weather';
	const FUNCTION_HOURLY_FORECATS = 'forecast';
	const FUNCTION_DAILY_FORECATS = 'forecast/daily';
	
	const TYPE_ID = 0;
	const TYPE_NAME = 1;
	const TYPE_COORDS = 2;
	
	const UNITS_IMPERIAL = 'imperial';
	const UNITS_METRIC = 'metric';
	
	public $name;
	
	public $coordinates;
	
	public $sun;
	
	public $temperature;
	
	public $humidity;
	
	public $pressure;
	
	public $wind;
	
	public $cloudy;
	
	public $status;
	
	public function current($query, $type, $units = null)
	{
		$url = self::BASE_URL.self::FUNCTION_CURRENT.'?';
		
		switch ($type)
		{
			case self::TYPE_ID:
				$url .= 'id='.$query;
				break;
			case self::TYPE_NAME:
				$url .= 'q='.$query;
				break;
			case self::TYPE_COORDS:
				$url .= 'lat='.$query['latitude'].'&lon='.$query['longitude'];
				break;
		}
		
		if ($units !== null)
		{
			$url .= '&units'.$units;
		}
		
		$request = curl_init();
		
		curl_setopt($request, CURLOPT_URL, $url);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		
		$response = CJSON::decode(curl_exec($request));
		
		$this->status = $response['cod'];
		
		if ($this->status == 200)
		{
			$this->name = $response['name'];

			$this->coordinates['lon'] = $response['coord']['lon'];
			$this->coordinates['lat'] = $response['coord']['lat'];
			
			$this->sun['rise'] = $response['sys']['sunrise'];
			$this->sun['set'] = $response['sys']['sunset'];

			$this->temperature = $response['main']['temp'];
			$this->humidity = $response['main']['humidity'];
			$this->pressure = $response['main']['pressure'];

			$this->wind['speed'] = $response['wind']['speed'];
			$this->wind['deg'] = $response['wind']['deg'];

			$this->cloudy = $response['clouds']['all'];
			
			return true;
		}

		return false;
	}
	
	public function currentById($id, $units = null)
	{
		return $this->current($id, self::TYPE_ID, $units);
	}
	
	public function currentByName($name, $units = null)
	{
		return $this->current($name, self::TYPE_NAME, $units);
	}
	
	public function currentByCoordinates($latitude, $longitude, $units = null)
	{
		return $this->current(array('latitude'=>$latitude, 'longitude'=>$longitude), self::TYPE_COORDS, $units);
	}
	
	public function daily($query, $date, $type, $units = null)
	{
		$days = ZDateTime::difference(ZDateTime::NOW, $date, ZDateTime::FORMAT_DAYS);
		
		if ($days <= 14)
		{
			$url = self::BASE_URL.self::FUNCTION_DAILY_FORECATS.'?';

			switch ($type)
			{
				case self::TYPE_ID:
					$url .= 'id='.$query;
					break;
				case self::TYPE_NAME:
					$url .= 'q='.$query;
					break;
				case self::TYPE_COORDS:
					$url .= 'lat='.$query['latitude'].'&lon='.$query['longitude'];
					break;
			}

			if ($units !== null)
			{
				$url .= '&units'.$units;
			}

			$request = curl_init();

			curl_setopt($request, CURLOPT_URL, $url);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

			$response = CJSON::decode(curl_exec($request));

			$this->status = $response['cod'];

			if ($this->status == 200)
			{
				$this->name = $response['city']['name'];

				$this->coordinates['lon'] = $response['city']['coord']['lon'];
				$this->coordinates['lat'] = $response['city']['coord']['lat'];

				$this->temperature = $response[$day]['temp']['day'];
				$this->humidity = $response[$day]['humidity'];
				$this->pressure = $response[$day]['pressure'];

				$this->wind['speed'] = $response[$day]['speed'];
				$this->wind['deg'] = $response[$day]['deg'];

				$this->cloudy = $response[$day]['clouds'];
				
				return true;
			}
		}
		
		return false;
	}
	
	public function dailyById($id, $time, $units = null)
	{
		return $this->daily($id, $time, self::TYPE_ID, $units);
	}
	
	public function dailyByName($name, $time, $units = null)
	{
		return $this->daily($name, $time, self::TYPE_NAME, $units);
	}
	
	public function dailyByCoordinates($latitude, $longitude, $time, $units = null)
	{
		return $this->daily(array('latitude'=>$latitude, 'longitude'=>$longitude), $time, self::TYPE_COORDS, $units);
	}
}

?>
